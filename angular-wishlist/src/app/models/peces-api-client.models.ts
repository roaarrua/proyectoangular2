import { Pez } from './pez.model';
import { BehaviorSubject, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoPezAction, PecesState } from './peces-state.models';
import { Injectable } from '@angular/core';

@Injectable()
export class PecesApiClient {
	peces: Pez[] = [];
  
	constructor(private store: Store<AppState>) {
		this.store
			.select(state => state.peces)
			.subscribe((data) => {
				console.log("peces sub store");
				console.log(data);
				this.peces = data.items;
			});
		this.store
			.subscribe((data) => {
				console.log("all store");
				console.log(data);
			});
	}
	
	add(p: Pez){
		this.store.dispatch(new NuevoPezAction(p));
	}

	elegir(p: Pez) {
		this.store.dispatch(new ElegidoFavoritoAction(p)); 
	}

	getById(id: string): Pez {
		return this.peces.filter(function(d) { return d.id.toString() === id; })[0];
	}

	getAll(): Pez[] {
		return this.peces;
	}
}