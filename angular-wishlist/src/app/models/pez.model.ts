import {v4 as uuid} from 'uuid';
import { UUID } from 'uuid-generator-ts';

export class Pez{
    selected: boolean;
    colores: string [];
    id = uuid();
    
    constructor(public nombre: string, public url: string, public votes: number = 0){
        this.colores = ['rojo', 'amarillo'];
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        if(this.votes>0)
        this.votes--;
    }
}
