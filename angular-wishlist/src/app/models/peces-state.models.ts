import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pez } from './pez.model';

//ESTADO
export interface PecesState{
    items: Pez[];
    loading: boolean;
    favorito: Pez;
}

export const initializePecesState = function (){
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

//ACCIONES
export enum PecesActionTypes {
    NUEVO_PEZ = '[Pez] Nuevo',
    ELEGIDO_FAVORITO = '[Pez] Favorito',
    VOTE_UP = '[Pez] Vote Up',
    VOTE_DOWN = '[Pez] Vote Down'
}

export class NuevoPezAction implements Action {
    type = PecesActionTypes.NUEVO_PEZ;
    constructor(public pez: Pez){}
}

export class ElegidoFavoritoAction implements Action {
    type = PecesActionTypes.ELEGIDO_FAVORITO;
    constructor(public pez: Pez){}
}

export class VoteUpAction implements Action {
    type = PecesActionTypes.VOTE_UP;
    constructor(public pez: Pez){}
}

export class VoteDownAction implements Action {
    type = PecesActionTypes.VOTE_DOWN;
    constructor(public pez: Pez){}
}

export type PecesActions = NuevoPezAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction;

//REDUCERS
export function reducerPeces(
    state: PecesState,
    action: PecesActions
) : PecesState {
    switch (action.type) {
        case PecesActionTypes.NUEVO_PEZ: {
            return {
                ...state,
                items: [...state.items, (action as NuevoPezAction).pez]
            };
        }
        case PecesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            const fav: Pez = (action as ElegidoFavoritoAction).pez;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
        case PecesActionTypes.VOTE_UP: {
            const p: Pez = (action as VoteUpAction).pez;
            p.voteUp();
            return { ...state };
        }
        case PecesActionTypes.VOTE_DOWN: {
            const p: Pez = (action as VoteDownAction).pez;
            p.voteDown();
            return { ...state };
        }
    }
    return state;
}

//EFFECTS
@Injectable()
export class PecesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(PecesActionTypes.NUEVO_PEZ),
        map((action: NuevoPezAction) => new ElegidoFavoritoAction(action.pez))
    );

    constructor(private actions$: Actions) {}
}