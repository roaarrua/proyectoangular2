import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { PecesApiClient } from './../../models/peces-api-client.models';
import { Pez } from './../../models/pez.model';
import { ActivatedRoute, provideRoutes } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';

class PecesApiClientViejo {
  getById(id: string): Pez {
    console.log('llamando por la clase vieja!');
    return null;
  }
}

interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@Injectable()
class PecesApiClientDecorated extends PecesApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
    super(store);
  }

  getById(id: string): Pez {
    console.log('llamando por la calse decorada');
    console.log('config: ' + this.config.apiEndPoint);
    return super.getById(id);
    }
}

@Component({
  selector: 'app-pez-detalle',
  templateUrl: './pez-detalle.component.html',
  styleUrls: ['./pez-detalle.component.css'],
  providers: [ 
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    { provide: PecesApiClient, useClass: PecesApiClientDecorated },
    { provide: PecesApiClientViejo, useExisting: PecesApiClient }
  ]
})
export class PezDetalleComponent implements OnInit {
  pez: Pez;

  constructor(private route: ActivatedRoute, private pecesApiClient: PecesApiClientViejo) {
   }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.pez = this.pecesApiClient.getById(id);
  }

}
