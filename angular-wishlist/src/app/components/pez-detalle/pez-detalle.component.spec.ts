import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PezDetalleComponent } from './pez-detalle.component';

describe('PezDetalleComponent', () => {
  let component: PezDetalleComponent;
  let fixture: ComponentFixture<PezDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PezDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PezDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
