import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Pez } from './../../models/pez.model';
import { PecesApiClient } from './../../models/peces-api-client.models';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoPezAction } from './../../models/peces-state.models';

@Component({
  selector: 'app-lista-peces',
  templateUrl: './lista-peces.component.html',
  styleUrls: ['./lista-peces.component.css'],
  providers: [ PecesApiClient ]
})
export class ListaPecesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Pez>;
  updates: string[];
  all;

  constructor(public pecesApiClient: PecesApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit(): void {
    this.store.select(state => state.peces.favorito)
    .subscribe(data => {
      const f = data;
        if (f != null) {
          this.updates.push('Se eligió: ' + f.nombre);
        }
    });
  }

  agregado(p: Pez) {
    this.pecesApiClient.add(p);
    this.onItemAdded.emit(p);
  }

  elegido(e: Pez) {
    this.pecesApiClient.elegir(e);
  }

  getAll() {}

}
