import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPecesComponent } from './lista-peces.component';

describe('ListaPecesComponent', () => {
  let component: ListaPecesComponent;
  let fixture: ComponentFixture<ListaPecesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPecesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
