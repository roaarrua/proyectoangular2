import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { VoteDownAction, VoteUpAction } from './../../models/peces-state.models';
import { Pez } from './../../models/pez.model'

@Component({
  selector: 'app-nombre-peces',
  templateUrl: './nombre-peces.component.html',
  styleUrls: ['./nombre-peces.component.css']
})
export class NombrePecesComponent implements OnInit {
  @Input() pez: Pez;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<Pez>;
  
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
   }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.pez);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.pez));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.pez));
    return false;
  }

}
