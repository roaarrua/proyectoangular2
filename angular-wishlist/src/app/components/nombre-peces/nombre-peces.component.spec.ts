import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NombrePecesComponent } from './nombre-peces.component';

describe('NombrePecesComponent', () => {
  let component: NombrePecesComponent;
  let fixture: ComponentFixture<NombrePecesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NombrePecesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NombrePecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
