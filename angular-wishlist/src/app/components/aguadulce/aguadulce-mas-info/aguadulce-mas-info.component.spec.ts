import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AguadulceMasInfoComponent } from './aguadulce-mas-info.component';

describe('AguadulceMasInfoComponent', () => {
  let component: AguadulceMasInfoComponent;
  let fixture: ComponentFixture<AguadulceMasInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AguadulceMasInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AguadulceMasInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
