import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AguadulceMainComponent } from './aguadulce-main.component';

describe('AguadulceMainComponent', () => {
  let component: AguadulceMainComponent;
  let fixture: ComponentFixture<AguadulceMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AguadulceMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AguadulceMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
