import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AguadulceDetalleComponent } from './aguadulce-detalle.component';

describe('AguadulceDetalleComponent', () => {
  let component: AguadulceDetalleComponent;
  let fixture: ComponentFixture<AguadulceDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AguadulceDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AguadulceDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
