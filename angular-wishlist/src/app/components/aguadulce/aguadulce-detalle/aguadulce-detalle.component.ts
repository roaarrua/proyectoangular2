import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-aguadulce-detalle',
  templateUrl: './aguadulce-detalle.component.html',
  styleUrls: ['./aguadulce-detalle.component.css']
})
export class AguadulceDetalleComponent implements OnInit {
  public id: any;

  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params => { this.id = params['id']; });
   }

  ngOnInit(): void {
  }

}
