import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNombrePecesComponent } from './form-nombre-peces.component';

describe('FormNombrePecesComponent', () => {
  let component: FormNombrePecesComponent;
  let fixture: ComponentFixture<FormNombrePecesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNombrePecesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNombrePecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
