import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax'
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Pez } from './../../models/pez.model'

@Component({
  selector: 'app-form-nombre-peces',
  templateUrl: './form-nombre-peces.component.html',
  styleUrls: ['./form-nombre-peces.component.css']
})
export class FormNombrePecesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Pez>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(private fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        //this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    });
    //observador de tipeo
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    })
   }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(() => ajax('/assets/datos.json'))
          ).subscribe(AjaxResponse => {
            this.searchResults = AjaxResponse.response;
          })
  }

  guardar(nombre: string, url: string): boolean {
    const d = new Pez(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean} {
    let l = control.value.toString().trim().length;
    if (l>0 && l<5) {
      return { invalidNombre: true };
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      let l = control.value.toString().trim().length;
      if (l>0 && l<minLong) {
        return { minLongNombre: true };
      }
      return null;
    }
  }
}
