import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NombrePecesComponent } from './components/nombre-peces/nombre-peces.component';
import { ListaPecesComponent } from './components/lista-peces/lista-peces.component';
import { PezDetalleComponent } from './components/pez-detalle/pez-detalle.component';
import { FormNombrePecesComponent } from './components/form-nombre-peces/form-nombre-peces.component';
import { initializePecesState, PecesEffects, PecesState, reducerPeces } from './models/peces-state.models';
import { environment } from 'src/environments/environment';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AguadulceComponent } from './components/aguadulce/aguadulce/aguadulce.component';
import { AguadulceMainComponent } from './components/aguadulce/aguadulce-main/aguadulce-main.component';
import { AguadulceMasInfoComponent } from './components/aguadulce/aguadulce-mas-info/aguadulce-mas-info.component';
import { AguadulceDetalleComponent } from './components/aguadulce/aguadulce-detalle/aguadulce-detalle.component';
import { ReservasModule } from './reservas/reservas.module'

export const childrenRoutesAguadulce: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: AguadulceMainComponent },
  { path: 'mas-info', component: AguadulceMasInfoComponent },
  { path: ':id', component: AguadulceDetalleComponent }
]
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaPecesComponent},
  { path: 'pez/:id', component: PezDetalleComponent},
  { path: 'login', component: LoginComponent},
  { 
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },
  {
    path: 'aguadulce',
    component: AguadulceComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesAguadulce 
  }
];

// Redux init
export interface AppState {
  peces: PecesState;
}

const reducers: ActionReducerMap<AppState> = {
  peces: reducerPeces
};

const reducersInitialState = {
  peces: initializePecesState()
}
// Redux fin init

@NgModule({
  declarations: [
    AppComponent,
    NombrePecesComponent,
    ListaPecesComponent,
    PezDetalleComponent,
    FormNombrePecesComponent,
    LoginComponent,
    ProtectedComponent,
    AguadulceComponent,
    AguadulceMainComponent,
    AguadulceMasInfoComponent,
    AguadulceDetalleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    AppRoutingModule,
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      }
    }),
    EffectsModule.forRoot([PecesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
